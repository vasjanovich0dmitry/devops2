from flask import Flask
import json


response1 = "Привет!"
response2 = "Пока!"
api = Flask(__name__)


@api.route('/hi', methods=['GET'])
def get_response1():
    return json.dumps(response1, ensure_ascii=False)


@api.route('/bye', methods=['GET'])
def get_response2():
    return json.dumps(response2, ensure_ascii=False)


if __name__ == '__main__':
    api.run()
